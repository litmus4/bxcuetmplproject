// Fill out your copyright notice in the Description page of Project Settings.


#include "BxcEngineSubsystem.h"
#include "Engine/Engine.h"
#include "AbilitySystemGlobals.h"
#include "Misc/CoreDelegates.h"

void UBxcEngineSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	if (GEngine && GEngine->IsInitialized())
	{
		UAbilitySystemGlobals* ASG = &UAbilitySystemGlobals::Get();
		ASG->InitGlobalData();
	}
	else
		DelePostEngineInitHandle = FCoreDelegates::OnPostEngineInit.AddUObject(this, &ThisClass::OnPostEngineInit);
}

void UBxcEngineSubsystem::Deinitialize()
{
	if (DelePostEngineInitHandle.IsValid())
	{
		FCoreDelegates::OnPostEngineInit.Remove(DelePostEngineInitHandle);
		DelePostEngineInitHandle.Reset();
	}
}

void UBxcEngineSubsystem::OnPostEngineInit()
{
	UAbilitySystemGlobals* ASG = &UAbilitySystemGlobals::Get();
	ASG->InitGlobalData();
}