// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BXCycleInstance.generated.h"

/**
 * 
 */
UCLASS()
class BUEGAME_API UBXCycleInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	virtual void Init() override;
	virtual void Shutdown() override;

	UPROPERTY(BlueprintReadWrite)
	bool bKeyboardRuntime = true;

private:
	void OnGameModeInitialized(AGameModeBase* GM);
};
