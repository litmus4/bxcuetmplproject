// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include <functional>
#include "BxcBlueprintLibrary.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBxcBPLib, Display, All)

/**
 * 
 */
UCLASS()
class BUEGAME_API UBxcBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	template<class T>
	static void ForEachDataTableRowValue(UDataTable* DataTable, std::function<void(const T*)>&& OnEach)
	{
		static_assert(TPointerIsConvertibleFromTo<T, FTableRowBase>::Value,
			"UBxcBlueprintLibrary ForEachDataTableRowValue static_assert");
		if (ensureAlways(IsValid(DataTable) && DataTable->GetRowStruct() && DataTable->GetRowStruct()->IsChildOf(T::StaticStruct())))
		{
			TMap<FName, uint8*>::TConstIterator Iter(DataTable->GetRowMap().CreateConstIterator());
			for (; Iter; ++Iter)
				OnEach((T*)Iter.Value());
		}
		else
		{
			UE_LOG(LogBxcBPLib, Error, TEXT("ForEachDataTableRowValue: UDT is invalid!"));
		}
	}
};

#define LOADTABLE_TMAP(DTPtr, RowClass, Container, IDMbr) UBxcBlueprintLibrary::ForEachDataTableRowValue<RowClass>(\
	DTPtr, [this](const RowClass* Row) {\
	check(!Container.Find(Row->IDMbr));\
	Container.Add(Row->IDMbr, *Row);\
})

#define LOADTABLE_MAP(DTPtr, RowClass, Container, IDClass, IDMbr) UBxcBlueprintLibrary::ForEachDataTableRowValue<RowClass>(\
	DTPtr, [this](const RowClass* Row) {\
	check(Container.find(Row->IDMbr) == Container.end());\
	Container.insert(std::pair<IDClass, RowClass>(Row->IDMbr, *Row));\
})