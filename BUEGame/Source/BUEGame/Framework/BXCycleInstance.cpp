// Fill out your copyright notice in the Description page of Project Settings.


#include "BXCycleInstance.h"
#include "GameFramework/GameModeBase.h"

void UBXCycleInstance::Init()
{
	//

	UE_LOG(LogTemp, Log, TEXT("@@@@@ BXCycleInstance top init end"));
	Super::Init();

	FGameModeEvents::GameModeInitializedEvent.AddUObject(this, &UBXCycleInstance::OnGameModeInitialized);
}

void UBXCycleInstance::Shutdown()
{
	//

	UE_LOG(LogTemp, Log, TEXT("@@@@@ BXCycleInstance shutdown end"));
	Super::Shutdown();
}

void UBXCycleInstance::OnGameModeInitialized(AGameModeBase* GM)
{
	//
}