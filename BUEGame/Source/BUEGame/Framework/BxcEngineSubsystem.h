// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/EngineSubsystem.h"
#include "BxcEngineSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class BUEGAME_API UBxcEngineSubsystem : public UEngineSubsystem
{
	GENERATED_BODY()
	
public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

protected:
	UFUNCTION()
	void OnPostEngineInit();

	FDelegateHandle DelePostEngineInitHandle;
};
