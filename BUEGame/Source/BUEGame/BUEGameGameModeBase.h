// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BUEGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BUEGAME_API ABUEGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
