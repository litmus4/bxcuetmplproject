// Copyright Epic Games, Inc. All Rights Reserved.

#include "BUEGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BUEGame, "BUEGame" );
