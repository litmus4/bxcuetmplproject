// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "OtherTableStructs.generated.h"

USTRUCT()
struct BUEGAME_API FGlobalParamRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Integer1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Float1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Integer2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Float2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FString> DescList;
};

USTRUCT()
struct BUEGAME_API FInputKeyRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName KeyName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayText;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMaterialInstance* MaterialInst;
};

USTRUCT()
struct BUEGAME_API FInputActionNameRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ActionName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayText;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bAxis;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bAxisPositive;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bGamepadModifyForbidden;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bKeyboardModifyForbidden;
};