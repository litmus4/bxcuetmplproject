// Fill out your copyright notice in the Description page of Project Settings.


#include "OtherTableCenter.h"
#include "../../Framework/BxcBlueprintLibrary.h"
#include "../BxcDataTableBoard.h"

COtherTableCenter::COtherTableCenter()
{
}

COtherTableCenter::~COtherTableCenter()
{
}

bool COtherTableCenter::Init()
{
	const UBxcDataTableBoard* Board = GetDefault<UBxcDataTableBoard>();
	check(Board);

	UDataTable* Table = Board->GlobalParamTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FGlobalParamRow, GlobalParamMap, ID);
	}

	Table = Board->InputKeyTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FInputKeyRow, InputKeyMap, ID);
		for (auto& Pair : InputKeyMap)
			InputKeyNameMap.Add(Pair.Value.KeyName, Pair.Key);
	}

	return true;
}

void COtherTableCenter::Release()
{
	GlobalParamMap.Empty();

	InputActionMap.Empty();
	InputActionNameMap.Empty();
	InputKeyMap.Empty();
	InputKeyNameMap.Empty();
}

FGlobalParamRow* COtherTableCenter::GetGlobalParamRow(int32 ID)
{
	return GlobalParamMap.Find(ID);
}

FName COtherTableCenter::LinkAxisName(const FName& AxisName, bool bPositiveDir)
{
	return FName(AxisName.ToString() + (bPositiveDir ? TEXT("1") : TEXT("0")));
}

bool COtherTableCenter::LoadInputActionNames()
{
	if (InputActionMap.Num() > 0)
		return false;

	const UBxcDataTableBoard* Board = GetDefault<UBxcDataTableBoard>();
	check(Board);
	UDataTable* Table = Board->InputActionNameTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FInputActionNameRow, InputActionMap, ID);
		for (auto& Pair : InputActionMap)
		{
			FName&& UniActionName = (Pair.Value.bAxis ?
				LinkAxisName(Pair.Value.ActionName, Pair.Value.bAxisPositive) : Pair.Value.ActionName);
			InputActionNameMap.Add(UniActionName, Pair.Key);
		}
		return true;
	}
	return false;
}

FInputActionNameRow* COtherTableCenter::GetInputActionNameRow(int32 ID)
{
	return InputActionMap.Find(ID);
}

FInputActionNameRow* COtherTableCenter::GetInputActionNameRowByName(const FName& ActionName)
{
	int32* ID = InputActionNameMap.Find(ActionName);
	if (ID)
		return InputActionMap.Find(*ID);
	return nullptr;
}

const TMap<int32, FInputActionNameRow>& COtherTableCenter::GetInputActionNameMap()
{
	return InputActionMap;
}

FInputKeyRow* COtherTableCenter::GetInputKeyRow(int32 ID)
{
	return InputKeyMap.Find(ID);
}

FInputKeyRow* COtherTableCenter::GetInputKeyRowByName(const FName& Name)
{
	int32* ID = InputKeyNameMap.Find(Name);
	if (ID)
		return InputKeyMap.Find(*ID);
	return nullptr;
}