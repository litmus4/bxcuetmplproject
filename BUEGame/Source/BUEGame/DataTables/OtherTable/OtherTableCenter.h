// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../Utilities/BxcUtil/Singleton.h"
#include "OtherTableStructs.h"

/**
 * 
 */
class BUEGAME_API COtherTableCenter : public Singleton<COtherTableCenter>
{
DECLARE_SINGLETON_CONSTRUCTOR(COtherTableCenter)
public:
	~COtherTableCenter();
	bool Init();
	void Release();

	FGlobalParamRow* GetGlobalParamRow(int32 ID);

	static FName LinkAxisName(const FName& AxisName, bool bPositiveDir);
	bool LoadInputActionNames();
	FInputActionNameRow* GetInputActionNameRow(int32 ID);
	FInputActionNameRow* GetInputActionNameRowByName(const FName& ActionName);
	const TMap<int32, FInputActionNameRow>& GetInputActionNameMap();

	FInputKeyRow* GetInputKeyRow(int32 ID);
	FInputKeyRow* GetInputKeyRowByName(const FName& Name);

private:
	TMap<int32, FGlobalParamRow> GlobalParamMap;

	TMap<int32, FInputActionNameRow> InputActionMap;
	TMap<FName, int32> InputActionNameMap;

	TMap<int32, FInputKeyRow> InputKeyMap;
	TMap<FName, int32> InputKeyNameMap;
};
