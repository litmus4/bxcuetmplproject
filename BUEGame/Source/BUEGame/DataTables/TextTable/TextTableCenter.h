// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../../Utilities/BxcUtil/Singleton.h"
#include "TextTableStructs.h"

/**
 * 
 */
class BUEGAME_API CTextTableCenter : public Singleton<CTextTableCenter>
{
DECLARE_SINGLETON_CONSTRUCTOR(CTextTableCenter)
public:
	~CTextTableCenter();
	bool Init(bool bUseCaptionTags = false);
	void Release();

	FTextRow* GetTextRow(int32 ID);
	FText GetText(int32 ID);
	FCaptionRow* GetCaptionRow(int32 ID);
	FText GetCaption(int32 ID);
	FText GetCaptionByTag(const FName& Tag);
	FStoryTextRow* GetStoryTextRow(int32 ID);
	FText GetStoryText(int32 ID);

private:
	TMap<int32, FTextRow> TextMap;
	TMap<int32, FCaptionRow> CaptionMap;
	TMap<FName, int32> CaptionTagMap;
	TMap<int32, FStoryTextRow> StoryTextMap;
};
