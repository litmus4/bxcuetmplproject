// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "TextTableStructs.generated.h"

USTRUCT()
struct BUEGAME_API FTextRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Text;
};

USTRUCT()
struct BUEGAME_API FCaptionRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Text;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName Tag;
};

USTRUCT()
struct BUEGAME_API FStoryTextRow : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Text;
};