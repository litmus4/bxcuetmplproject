// Fill out your copyright notice in the Description page of Project Settings.


#include "TextTableCenter.h"
#include "../../Framework/BxcBlueprintLibrary.h"
#include "../BxcDataTableBoard.h"

CTextTableCenter::CTextTableCenter()
{
}

CTextTableCenter::~CTextTableCenter()
{
}

bool CTextTableCenter::Init(bool bUseCaptionTags)
{
	const UBxcDataTableBoard* Board = GetDefault<UBxcDataTableBoard>();
	check(Board);

	UDataTable* Table = Board->TextTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FTextRow, TextMap, ID);
	}

	Table = Board->CaptionTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FCaptionRow, CaptionMap, ID);
	}
	if (bUseCaptionTags)
	{
		for (auto& Pair : CaptionMap)
			CaptionTagMap.Add(Pair.Value.Tag, Pair.Key);
	}

	Table = Board->StoryTextTable.LoadSynchronous();
	if (Table)
	{
		LOADTABLE_TMAP(Table, FStoryTextRow, StoryTextMap, ID);
	}
	
	return true;
}

void CTextTableCenter::Release()
{
	TextMap.Empty();
	CaptionMap.Empty();
	CaptionTagMap.Empty();
	StoryTextMap.Empty();
}

FTextRow* CTextTableCenter::GetTextRow(int32 ID)
{
	return TextMap.Find(ID);
}

FText CTextTableCenter::GetText(int32 ID)
{
	FTextRow* Row = TextMap.Find(ID);
	if (Row)
		return Row->Text;
	return FText::GetEmpty();
}

FCaptionRow* CTextTableCenter::GetCaptionRow(int32 ID)
{
	return CaptionMap.Find(ID);
}

FText CTextTableCenter::GetCaption(int32 ID)
{
	FCaptionRow* Row = CaptionMap.Find(ID);
	if (Row)
		return Row->Text;
	return FText::GetEmpty();
}

FText CTextTableCenter::GetCaptionByTag(const FName& Tag)
{
	int32* ID = CaptionTagMap.Find(Tag);
	if (ID)
	{
		FCaptionRow* Row = CaptionMap.Find(*ID);
		if (Row)
			return Row->Text;
	}
	return FText::GetEmpty();
}

FStoryTextRow* CTextTableCenter::GetStoryTextRow(int32 ID)
{
	return StoryTextMap.Find(ID);
}

FText CTextTableCenter::GetStoryText(int32 ID)
{
	FStoryTextRow* Row = StoryTextMap.Find(ID);
	if (Row)
		return Row->Text;
	return FText::GetEmpty();
}