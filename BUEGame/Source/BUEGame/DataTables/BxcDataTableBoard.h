// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "Engine/DataTable.h"
#include "BxcDataTableBoard.generated.h"

/**
 * 
 */
UCLASS(Config = Game, BlueprintType, NotBlueprintable)
class BUEGAME_API UBxcDataTableBoard : public UDeveloperSettings
{
	GENERATED_BODY()
	
public:
	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Text)
	TSoftObjectPtr<UDataTable> TextTable;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Text)
	TSoftObjectPtr<UDataTable> CaptionTable;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Text)
	TSoftObjectPtr<UDataTable> StoryTextTable;

public:
	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Other)
	TSoftObjectPtr<UDataTable> GlobalParamTable;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Other)
	TSoftObjectPtr<UDataTable> InputActionNameTable;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = Other)
	TSoftObjectPtr<UDataTable> InputKeyTable;
};
