// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
template<class T>
class BUEGAME_API Singleton
{
public:
	~Singleton() {}

	static T* GetInstance()
	{
		if (!InstPtr)
		{
			InstPtr = new T();
		}
		return InstPtr.Get();
	}

protected:
	Singleton() {}

	static TSharedPtr<T> InstPtr;
};

#define DECLARE_SINGLETON_CONSTRUCTOR(ClassName) private:\
	ClassName();\
	friend class Singleton<ClassName>;

#define DEFINE_SINGLETON_CONSTRUCTOR(ClassName) private:\
	friend class Singleton<ClassName>;\
	ClassName()